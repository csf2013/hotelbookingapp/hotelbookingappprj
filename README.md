# Hotel Booking App
## Ideal situation:
Ideally, Hotels should be able to track all hotel booking transactions
automatically.
## Reality:
Currently, We do not have any effective Hotel booking platform apps.
## Consequences:
It is very difficult to do online business and manual work is involved
between the merchant and customers.
## Proposal:
We propose to have a team to look into an IT solution so that all it is
easy to book hotel rooms and to create a delightful platform and
storytelling experience for the users.
## Project Title: Hotel Booking App: An Online Room Booking Application
## Project Goals: To create an hotel booking web app using React
framework in 15 weeks.
## Project Scope:
- [ ] Creation of Login and Registration of User
- [ ] Implement User management
- [ ] Stripe onboarding and payment settings
- [ ] Create and Display Hotels
- [ ] Implement CRUD for Hotels
- [ ] Booking a hotel and checkout with stripe
- [ ] Implement Hotels Search
## Out of Scope:
- [ ] Email Notification to Users when registration.
## Objectives:
- [ ] Create a usable Web application.
- [ ] Display the information of all the hotels and book seamlessly.
- [ ] Create easy to navigate navigation and messages.
## Technology Stack:
- JavaScript
- Node js
- Express Js
- PostgreSQL
- JWT
- React
- React Bootstrap
- Redux
- Stripe
## Usage
## Install Dependencies
```
npm install
cd frontend
npm install
```
### Run
```
# Run frontend
npm run client
# Run backend
npm run server
```
- Version: 1.0.0
- License: GCIT
- Author: Ms. Pema Tshoki