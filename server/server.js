// console.log("==> Node Server")
import express from "express"
// import router from './routes/auth'
// import fs from 'fs'
import { readdirSync } from 'fs'
const cors = require('cors');

const morgan = require("morgan");

require('dotenv').config();


// const express = require("express")

const app = express();
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());

const { Pool } = require('pg');
// PostgreSQL connection configuration

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'postgres',
    port: 5432, // Default PostgreSQL port
});

// Test the database connection
pool.query('SELECT NOW()', (err, res) => {
    if (err) {
        console.error('Error connecting to the database:', err);
    } else {
        console.log('Connected to the database:', res.rows[0].now);
    }
});
// router at the root path
/*app.get("/:message", (req, res) => {
res.status(200).send(req.params.message);
});*/
// app.use("/", router);
// fs.readdirSync('./routes').map(
//     (r) => app.use('/', require(`./routes/${r}`))
// );
readdirSync('./routes').map(
    (r) => app.use('/', require(`./routes/${r}`))
);

const port = process.env.PORT || 8000;

// app.listen(8000, () => console.log('Server is running on port 8000'))
app.listen(port, () => console.log(`Server is running on port ${port}`));
