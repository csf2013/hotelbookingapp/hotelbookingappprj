// import { DataTypes, Sequelize } from 'sequelize';
// import bcrypt from 'bcrypt';
const { DataTypes, Sequelize } = require('sequelize');
// const sequelize = require('../db/connection');
import bcrypt from 'bcrypt';


// Create a Sequelize instance and connect to your PostgreSQL database
const sequelize = new Sequelize('HotelBooking', 'postgres', 'postgres', {
    host: '127.0.0.1',
    dialect: 'postgres',
});
// Define the User model with a schema
const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
        },
    },
    stripe_account_id: DataTypes.STRING,
    stripe_seller: DataTypes.JSONB,
    stripeSession: DataTypes.JSONB,
}, {
    timestamps: true, // This will add createdAt and updatedAt fields to the model
    hooks: {
        beforeCreate: async (user) => {
            if (user.password) {
                const saltRounds = 12;
                const hash = await bcrypt.hash(user.password, saltRounds);
                user.password = hash;
            }
        },
        beforeUpdate: async (user) => {
            if (user.changed('password')) {
                const saltRounds = 12;
                const hash = await bcrypt.hash(user.password, saltRounds);
                user.password = hash;
            }
        },
    },

});

// Define a method to compare passwords on the User model
User.prototype.comparePassword = function (password, next) {
    bcrypt.compare(password, this.password, function (err, match) {
        if (err) {
            console.log("compare password Err", err);
            return next(err, false);
        }
        console.log("match password", match);
        return next(null, match);
    });
};

// Synchronize the model with the database (create the table if it doesn't
// exist)
sequelize.sync();
export default User;
