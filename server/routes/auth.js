import express from 'express'
const router = express.Router()

//import controllers
import { showMessage, register, login } from '../controllers/auth';
// router.get("/:message", (req, res) => {
//     res.status(200).send(req.params.message);
// });
router.get("/:message", showMessage);
router.post('/register', register);
router.post('/login', login);

// export default router;
module.exports = router;