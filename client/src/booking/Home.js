import React from 'react'
import { useSelector } from 'react-redux/es/hooks/useSelector'
import Topnav from '../Components/Topnav'

function Home() {
    const user = useSelector((state) => state.user)
    return (
        <div>
            <Topnav />
            <div className='container-fluid h1 p5 text-center'>

                Home
                {JSON.stringify(user)}
            </div>
        </div>
    )
}

export default Home