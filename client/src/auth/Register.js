import React, { useState } from 'react';
import { toast } from 'react-toastify';
import RegisterForm from '../Components/RegisterForm'; // Assuming RegisterForm is a default export
import { useNavigate } from 'react-router-dom';
// Assuming register is an async function from the actions/auth module
import { register } from '../actions/auth';

const Register = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const res = await register({
                name,
                email,
                password,
            });

            console.log('Register User ==>', res);
            toast.success('Registration successful. Login NOW!');

            // Redirect to the login page
            navigate('/login');
        } catch (err) {
            console.log('Axios Error:', err);

            if (err.response && err.response.status === 400) {
                toast.error(err.response.data);
            }
        }
    };

    return (
        <>
            <div className="container-fluid h1 p5 text-center">Register</div>
            <div className="container">
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <RegisterForm
                            handleSubmit={handleSubmit}
                            name={name}
                            setName={setName}
                            email={email}
                            setEmail={setEmail}
                            password={password}
                            setPassword={setPassword}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};

export default Register;
