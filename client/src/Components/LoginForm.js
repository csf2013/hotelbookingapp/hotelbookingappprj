const LoginForm = ({ handleSubmit, email, setEmail, password, setPassword }) => {
    const isValidEmail = (email) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    };

    const isStrongPassword = (password) => {
        return password.length > 6;
    };

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    return (
        <div className="container" >
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <div className="user-input-box">
                        <label htmlFor="email"></label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Enter Email"
                            value={email}
                            onChange={handleEmailChange}
                            className={`form-control m-3 ${(!isValidEmail(email) && email) && 'is-invalid'}`}
                        />
                        {(!isValidEmail(email) && email) && (
                            <div className="invalid-feedback">Enter a valid email address.</div>
                        )}
                    </div>

                    <div className="user-input-box">
                        <label htmlFor="password"></label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Enter Password"
                            value={password}
                            onChange={handlePasswordChange}
                            className={`form-control m-3 ${(!isStrongPassword(password) && password) && 'is-invalid'}`}
                        />
                        {(!isStrongPassword(password) && password) && (
                            <div className="invalid-feedback">Password should be at least 7 characters.</div>
                        )}
                    </div>
                </div>

                <button type="submit" className="btn btn-primary m-3">
                    Submit
                </button>
            </form>
        </div>
    );
};

export default LoginForm;
