
import React from "react";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "./RegisterForm.css"

const RegisterForm = ({
    handleSubmit,
    name,
    setName,
    email,
    setEmail,
    password,
    setPassword,
    passwordStrength,
}) => {
    const showError = (message) => {
        toast.error(message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
        });
    };

    const handleNameChange = (e) => {
        const newName = e.target.value;
        setName(newName);
        if (!newName.trim()) {
            showError('Name is required.');
        }
    };

    const handleEmailChange = (e) => {
        const newEmail = e.target.value;
        setEmail(newEmail);
        if (!newEmail.trim()) {
            showError('Email is required.');
        }
    };

    const handlePasswordChange = (e) => {
        const newPassword = e.target.value;
        setPassword(newPassword);
        if (!newPassword.trim()) {
            showError('Password is required.');
        }
    };
    const isNameValid = (name) => /^[A-Za-z\s]{3,30}$/.test(name);
    const isValidEmail = (email) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    };

    const isStrongPassword = (password) => {
        return password.length > 6;
    };


    return (

        <div className="container">

            <form onSubmit={handleSubmit}>
                <div className="">
                    <h2 class="title">Register</h2>
                    <div class="input-field">
                        <label htmlFor="fullName"></label>
                        <input
                            type="text"
                            id="fullName"
                            name="fullName"
                            placeholder="Enter Full Name"
                            value={name}
                            onChange={handleNameChange}
                            className={`form-control m-3 ${(!isNameValid(name)) && 'is-invalid'}`}
                        />
                    </div>
                    <div class="input-field">
                        <label htmlFor="email"></label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Enter Email"
                            value={email}
                            onChange={handleEmailChange}
                            className={`form-control m-3 ${(!isValidEmail(email)) && 'is-invalid'}`}
                        />

                    </div>
                    <div class="input-field">
                        <label htmlFor="password"></label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Enter Password"
                            value={password}
                            onChange={handlePasswordChange}
                            className={`form-control m-3 ${(!isStrongPassword(password)) && 'is-invalid'}`}
                        />
                    </div>

                    <div className="strength-meter">{passwordStrength}</div>
                </div>
                <div className="form-submit-btn">
                    <input className="  btn btn-primary m-3" type="submit" value="Register" />
                </div>
            </form>
        </div >

    );

}

    ;

export default RegisterForm;
