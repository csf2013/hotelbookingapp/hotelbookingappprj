import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import Login from "./auth/Login"
import Register from "./auth/Register"
import Home from "./booking/Home"
import DashboardSeller from "./user/DashboardSeller";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PrivateRoute from "./Components/PrivateRoute";
import Dashboard from "./user/Dashboard";
import Topnav from "./Components/Topnav";
import NewHotel from "./hotels/NewHotel";
function App() {
  return (
    <Router>

      <Topnav />
      <ToastContainer position="top-center" />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route exact path="/dashboard" element={<PrivateRoute><Dashboard /></PrivateRoute>} />
        <Route exact path="/dashboard/seller" element={<PrivateRoute><DashboardSeller
        /></PrivateRoute>} />
        <Route
          exact
          path="/hotels/new"
          element={<PrivateRoute><NewHotel /></PrivateRoute>}
        />
      </Routes>
    </Router>
  );
}

export default App;
