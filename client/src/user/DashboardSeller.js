import DashboardNav from "../Components/DashboardNav";
import ConnectNav from "../Components/ConnectNav";
import { Link } from 'react-router-dom'
const DashboardSeller = () => {
    return (
        <>
            <div className="conatiner-fluid bg-secondary p-4">
                <ConnectNav />
            </div>
            <div className="container-fluid p-2">
                <DashboardNav />
            </div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-10 col-md-9 col-sm-8">
                        <h2>Your Hotels</h2>
                    </div>
                    <div className="col-lg-2 col-md-3 col-sm-4">
                        <Link to="/hotels/new" className="btn btn-primary">
                            +Add New
                        </Link>
                    </div>
                </div>
            </div>
        </>
    );
};
export default DashboardSeller;